const http = require('http');

const port = 3000;

const server = http.createServer((request, response) => {
    if (request.url == '/login') {
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end("You are currently in the login page");
    }
    else if (request.url == '/homepage') {
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end("This is Homepage!");

    }
    else {
        response.writeHead(404, { 'Content-Type': 'text/plain' });
        response.end("This is not available!");
    }



});

server.listen(port);
console.log(`Server is now running at localhost: ${port}`);